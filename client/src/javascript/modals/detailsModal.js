import View from './../views/view';

export default class DetailsModal extends View {
	constructor() {
		super();

		this.modal = document.getElementById('modal');
	}

	modalContainer;

	async show(fighterInfoPromise) {
		fighterInfoPromise
			.then(fighterInfo => {
				this._display(fighterInfo);

				const event = new CustomEvent('close', { 'detail': fighterInfo });
				Array.from(document.getElementsByClassName('close')).forEach(b => b.addEventListener('click', () => {
					this.modal.style.display = 'none';
					this.modal.dispatchEvent(event)
				}));
			})
			.catch(err => {
				console.error(err);
				this.modal.style.display = 'block';
				this.modal.innerHTML = 'Failed to load data';
			})
	}

	_display(details) {
		if (this.modalContainer) {
			this.modal.removeChild(this.modalContainer);
			this.modalContainer = null;
		}

		this.modal.style.display = 'block';

		const closeButton = this.createElement({ tagName: 'span', className: 'close' });
		closeButton.innerHTML = '&times;';

		const title = this._createTitle(details.name);
		const content = this._createInputs(details);

		const subButton = this.createElement({
			tagName: 'input',
			className: 'close',
			attributes: { type: 'submit', id: 'submit', value: 'Ok' }
		});
		const buttonContainer = this.createElement({ tagName: 'div', className: 'right-bar' });
		buttonContainer.append(subButton);

		this.modalContainer = this.createElement({ tagName: 'div', className: 'content' });
		this.modalContainer.append(closeButton, title, content, buttonContainer);
		this.modal.append(this.modalContainer);
	}

	_createTitle(name) {
		const container = this.createElement({ tagName: 'div', className: 'mid' });

		const titlename = this.createElement({ tagName: 'span', className: 'title' });
		titlename.innerHTML = 'name: ';

		const titleField = this.createElement({ tagName: 'span', className: 'title' });
		titleField.innerHTML = name;

		container.append(titlename, titleField);

		return container;
	}

	_createInputs(details) {
		const detailsHolder = this.createElement({ tagName: 'div', className: 'table' });
		['health', 'attack', 'defense'].forEach(f => {
			detailsHolder.append(
				this._createFieldName(f),
				this._createField(details[f], e => details[f] = parseInt(e.target.value))
			);
		})

		return detailsHolder;
	}

	_createFieldName(name) {
		const label = this.createElement({ tagName: 'div' });
		label.innerHTML = name;
		return label;
	}

	_createField(value = '', onChange) {
		const input = this.createElement({ tagName: 'input', attributes: { type: 'number', value, min: 1 } });
		input.addEventListener('mouseup', onChange);
		return input;
	}
}
