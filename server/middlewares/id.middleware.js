const { getDataArray } = require('./../services/fighter.service');

const validId = (req, res, next) => {
	if (
		req &&
		req.params &&
		req.params.id
	) {
		const reg = /^\d+$/;
		if (reg.test(req.params.id)) {
			const { params: { id } } = req;
			const fighters = getDataArray();
			if(fighters.filter(item => item._id === id).length > 0) {
				return next();
			}
		}
	}
	res.status(404).end('invalid id');
}

module.exports = {
	validId
}