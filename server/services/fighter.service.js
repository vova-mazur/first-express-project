const { getData, saveData } = require('./../repositories/user.repository');

const getDataArray = () => {
	const fileContent = getData();

	if (fileContent) {
		let fighters = Array.from(JSON.parse(fileContent));

		return fighters;
	} else {
		return null;
	}
}

const getFighterById = id => {
	if (id) {
		const fighters = getDataArray();
		const result = fighters.find(item => item._id === id);

		return result;
	} else {
		return null;
	}
}

const saveNewFighter = fighter => {
	if (fighter) {
		const fighters = getDataArray();
		fighters.push(fighter);

		return saveData(fighters);
	} else {
		return false;
	}
}

const putAndSaveFighter = (id, fighter) => {
	if (id && fighter) {
		const fighters = getDataArray();
		const index = fighters.findIndex(item => item._id === id);
		fighters[index] = fighter;

		return saveData(fighters)
	} else {
		return false;
	}
};

const deleteFighterById = id => {
	if (id) {
		const fighters = getDataArray();
		const newFighters = fighters.filter(item => item._id !== id);

		return saveData(newFighters);
	} else {
		return false;
	}
}

module.exports = {
	getFighterById,
	saveNewFighter,
	putAndSaveFighter,
	deleteFighterById,
	getDataArray
}
