const btoa = require('btoa');

const encryptData = data => {
	if (data) {
		return btoa(data);
	} else {
		return null;
	}
}

const getEncryptedData = data => {
	if (data) {
		const string = JSON.stringify(data, null, 2);
		const encryptedData = encryptData(string);

		return encryptedData;
	} else {
		return null;
	}
}

module.exports = {
	encryptData,
	getEncryptedData
}