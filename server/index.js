const express = require('express');
const http = require('http');
const cors = require('cors');
const bodyParser = require('body-parser')

const fightersRouter = require('./routes/fighters');

const app = express();

app.use(cors());

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.set('port', process.env.PORT || 80);

app.get('/', (req, res) => {
	res.end('Welcome');
})

app.use('/', fightersRouter);

http.createServer(app).listen(app.get('port'), () => {
	console.log('Express HTTP server listening on port ' + app.get('port'));
});