const fs = require('fs');
const path = require('path');

const { getEncryptedData } = require('./../services/encrypt.service');

const filePath = path.join(__dirname, '/../data', 'userList.json');

const getData = () => {
	const fileContent = fs.readFileSync(filePath);

	if (fileContent) {
		return fileContent;
	} else {
		return null;
	}
};

const saveData = data => {
	if (data) {
		const content = JSON.stringify(data, null, 2);
		fs.writeFileSync(filePath, content);
		return true;
	} else {
		return false;
	}
}

module.exports = {
	getData,
	saveData
};