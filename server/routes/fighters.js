const express = require('express');
const router = express.Router();

const { validId } = require('./../middlewares/id.middleware');
const { getData } = require('./../repositories/user.repository');
const { encryptData, getEncryptedData } = require('./../services/encrypt.service');
const { getFighterById, saveNewFighter, putAndSaveFighter, deleteFighterById } = require('./../services/fighter.service');

router.use('/fighters/:id', validId);

router.get('/fighters', (req, res) => {
	const content = getData();
	const encryptedContent = encryptData(content);

	if (encryptedContent) {
		res.status(200).json(encryptedContent);
	} else {
		res.status(404).send('Data error');
	}
});

router.get('/fighters/:id', (req, res) => {
	const { params: { id } } = req;

	const fighter = getFighterById(id);
	const encryptedContent = getEncryptedData(fighter);

	if (encryptedContent) {
		res.status(200).json(encryptedContent);
	} else {
		res.status(400).send('Data error');
	}
})

router.post('/fighters', (req, res) => {
	const result = saveNewFighter(req.body);

	if (result) {
		res.status(200).send({ success: 'success' });
	} else {
		res.status(500).send('Database error');
	}
});

router.put('/fighters/:id', (req, res) => {
	const { params: { id } } = req;
	const result = putAndSaveFighter(id, req.body);

	if (result) {
		res.status(200).send({ success: 'success' });
	} else {
		res.status(500).send('Database error');
	}
});

router.delete('/fighters/:id', (req, res) => {
	const { params: { id } } = req;

	const result = deleteFighterById(id);
	if (result) {
		res.status(200).send({ success: 'success' });
	} else {
		res.status(500).send('Database error');
	}
});

module.exports = router